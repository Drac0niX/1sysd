#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[]) {
    // argc = argument count ; combien d'argument j'ai reçu
    // argv = arguments values ; quels sont-il

    for (int i = 0; i < argc; i++) {
        printf("Argument %d : %s\n", i, argv[i]);
    }

    exit(0);

}

