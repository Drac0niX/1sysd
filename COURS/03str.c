#include<stdio.h>
#include<stdlib.h>

int main() {
    char t1[] = "Hello"; 
    char t2[] = { 'H', 'e', 'l', 'l', 'o', 0 };
    char t3[] = { 72, 101, 108, 108, 111, 0 };

    printf("%s\n", t1);
    printf("%s\n", t2);
    printf("%s\n", t3);
    
    for (int i = 0; t1[i]; i++) {
        printf("i = %d ; char = %c, code = %d\n", i, t1[i], t1[i]);
    }

    exit(0);
}

