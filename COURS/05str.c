#include<stdio.h>
#include<stdlib.h>

int main() {
    char t1[] = "Hello"; 
    char t2[] = { 'H', 'e', 'l', 'l', 'o', 0 };
    char t3[] = { 72, 101, 108, 108, 111, 0 };
    char *p;

    // printf("%s\n", t1);
    // printf("%s\n", t2);
    // printf("%s\n", t3);
   
    p = t1;
    while (*p) {
        printf("%lx\n", p);
        printf("%c\n", *p);
        p++;
    }

    exit(0);
}

