#include<stdio.h>
#include<stdlib.h>

int main() {
    int s = 0, nb;
    int tab[50];

    printf("Nombre d'élément (entre 1 et 50) : ");
    scanf("%d", &nb);

    for (int i = 0; i < nb; i++) {
        printf("Entrez un nombre : ");
        scanf("%d", &tab[i]);
        s += tab[i];
    }
    for (int i = 0; i < nb; i++) {
        printf("%d ", tab[i]);
    }
    printf("\nSomme : %d\n", s);
    exit(0);
}

