#include<stdio.h>
#include<stdlib.h>

int main() {
    int s = 0, nb;
    int tab[50];
    int min, max;

    printf("Nombre d'éléments (entre 1 et 50) : ");
    scanf("%d", &nb);

    for (int i = 0; i < nb; i++) {
        printf("Entrez un nombre : ");
        scanf("%d", &tab[i]);
        s += tab[i];
    }
    for (int i = 0; i < nb; i++) {
        printf("%d ", tab[i]);
    }
    printf("\nSomme : %d\n", s);
    printf("Moyenne : %.2f\n", (double)s/(double)nb);
    // plus grand, plus petit
    // en supposant que le tableau a au moins un élément
    min = tab[0];
    max = tab[0];
    for (int i = 1; i < nb; i++) {
        if (tab[i] > max) {
            max = tab[i];
        }
        if (tab[i] < min) {
            min = tab[i];
        }
    }
    printf("Plus grand élément : %d\n", max);
    printf("Plus petit élément : %d\n", min);

    exit(0);
}

