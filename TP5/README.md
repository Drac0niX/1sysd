Que font les fonctions quizz1 et quizz2 ?
Premier point : Si p est un pointeur vers un caractère
(char *p) qui, initialement, pointe vers le début
d'une chaîne de caractère, i.e.

p -----> |H|e|l|l|o| |W|o|r|l|d|0|


(c'est vraiment 0 = zéro en dernier !)
Que valent et que font respectivement :

*p
*p++
*++p

Quand *p++ est-il nul ?
