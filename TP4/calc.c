#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[]) {
    // argc = argument count ; combien d'argument j'ai reçu
    // argv = arguments values ; quels sont-il
    double x , y;
    char op;
    op = argv[3][0];
    x = strtof(argv[1], NULL);
    y = strtof(argv[2], NULL);
    switch (op) {
        case '+':
            printf("Résultat : %.2f\n", x + y);
            break;
        case '-':
            printf("Résultat : %.2f\n", x - y);
            break;
        case '*':
            printf("Résultat : %.2f\n", x * y);
            break;
        case '/':
            printf("Résultat : %.2f\n", x / y);
            break;
        default:
            printf("Opération inconnue.\n");
    }
    exit(0);
}


