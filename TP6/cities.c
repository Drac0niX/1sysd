#include<stdlib.h>
#include<stdio.h>

typedef struct City City;
struct City {
    char name[51]; // nom de la ville, max 50 caractères
    int pop;
    double area; 
};

City askcity() {
    City city;

    printf("Nom : ");
    scanf("%s", city.name);
    printf("Population : ");
    scanf("%d", &city.pop);
    printf("Surface : ");
    scanf("%lf", &city.area);

    return city;
}

void printcity(City city) {
    printf("Ville : %s, population : %d, surface : %.2lf\n", \
             city.name, city.pop, city.area);
}

double density(City city) {
    return city.area / city.pop;
}

int main() {
    City city1 = { "Versailles", 83587, 26.18 };

    printcity(city1);
    // TODO : pourquoi 0 ?
    printf("Densité : %.2f\n", density(city1));

    city1 = askcity();

    printcity(city1);

    exit(0);
}



