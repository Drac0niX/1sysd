#include<stdio.h>
#include<stdlib.h>

float celsius2fahrenheit(float c) {
    return (9/5) * c+32;
}

float fahrenheit2celsius(float f) {
    return (f-32) * 5/9;
}

int main() {
    int rep;
    float temp;
    
    printf("(1) Conversion de Degrés Celsius à Fahrenheit , (2) Conversion de Fahrenheit à Degrés Celsius : ");
    scanf("%d", &rep);
    
    if (rep==1) {
    	printf("Température en Degrés Celsius : ");
    	scanf("%f", &temp);
    	printf("%.2f Degrés Celsius = %.2f Fahrenheit\n", temp, celsius2fahrenheit(temp));
    	}
    else if (rep==2) {
    	printf("Température en Fahrenheit : ");
    	scanf("%f", &temp);
    	printf("%.2f Fahrenheit = %.2f Degrés Celsius\n", temp, fahrenheit2celsius(temp));
    	}
    else {
    	printf("Entrée non valide.\n");
    	}
    
    exit(0);
}
