#include <stdio.h>
#include <string.h>

void reverse_string(char* mot) {
    int l = strlen(mot);
    for (int i = 0; i < l / 2; i++) {
        char temp = mot[i];
        mot[i] = mot[l-1-i];
        mot[l-1-i] = temp;
        }
}


int main(int argc, char *argv[]) {
    for (int i = argc-1 ; i>0 ; i--) {
        reverse_string(argv[i]);
        printf("%s\n", argv[i]);
        }
    return 0;
}
