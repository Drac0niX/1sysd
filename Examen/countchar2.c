#include <stdio.h>
#include <string.h>
#include <ctype.h>

int count_char(char* chaine, char car , int argopt) {
    int count = 0;
    
    if (argopt) {
        char min = tolower(car);
        for (int i = 0; i < strlen(chaine); i++) {
            if (tolower(chaine[i]) == min) {
                count++;
                }
            }
        }
        
    else {
        for (int i = 0; i < strlen(chaine); i++) {
            if (chaine[i] == car) {
                count++;
                }
            }
        }
    return count;
    }

int main(int argc, char* argv[]) {
    char* chaine = argv[1];
    char car = argv[2][0];
    int argopt = 0;
    
    if (argc > 3 && strcmp(argv[3], "-i") == 0) {
        argopt = 1;
        }

    printf("%d\n", count_char(chaine, car , argopt));
    return 0;

}
