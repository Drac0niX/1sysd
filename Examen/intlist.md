- Quel problème constatez-vous en exécutant le programme ? (note : vous pouvez l'interrompre par CTRL-C)

Le programme tourne à l'infini.


- Quel est la raison de ce problème ?

C'est la ligne de code "head->next->next->next->next = head->next;" qui fait ce problème : la cinquième position pointe vers la deuxième, ce qui fait une boucle infinie.

